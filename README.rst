# atl-hello-world-component # 

This is a simple example of atl-django-component.

## How to use it? ##

You have 2 form of use the django-components:

1. Create the component instance in the app view, put it in context and execute it in template page
2. Instantiate and execute it in the template page

View [atl-hello-world-component documentation](https://bitbucket.org/atlantesoftware/atl-hello-world-component)